package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class StackImplementation implements StackOperations {

    private List<String> list  = new ArrayList<String>();


    @Override
    public List<String> get() {
        return list;
    }

    @Override
    public Optional<String> pop() {
        try{
            Optional<String> name = Optional.ofNullable(list.get(list.size()-1));
            list.remove(list.size()-1);
            return name;
        }catch (Exception e){
            return Optional.empty();
        }
    }

    @Override
    public void push(String item) {
        list.add(item);
    }
}
